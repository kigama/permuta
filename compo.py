import itertools
import csv


#seleccion elemento cadena
def elemento(elemento1,h):
    #print('elemento',h,1+elemento1.index(h))
    return 1+elemento1.index(h)

def compo(cad1):
    with open('hello.csv', 'w') as csvfile:
        fieldnames = ['cadena 1', 'cadena2', 'a', 'b', 'c','d','e']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        contador=0
        for i in range(120):
            a,b,c,d,e= cad1[i]
            #print('a',a,'b',b,'c',c,'d',d,'e',e)
            for j in range(120):
                cadena2= cad1[j]
                #print(i, cad1[i], j, cadena2)
                a1 = elemento(cadena2,a)
                b1 = elemento(cadena2,b)
                c1 = elemento(cadena2,c)
                d1 = elemento(cadena2,d)
                e1 = elemento(cadena2,e)
                writer.writerows([{'cadena 1':cad1[i], 'cadena2':cadena2, 'a':a1, 'b':b1, 'c':c1,'d':d1,'e':e1}])
                #print(a1,b1,c1,d1,e1)
                
                contador=contador+1

    print('contador ',contador)        
    




#main
a = [1,2,3,4,5]
#lista de 120, por que son 5 elementos permutados
b=list(itertools.permutations(a))


compo(b)

#Fin main