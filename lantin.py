




        
    
      
def row(n, r, c = []):
   if len(c) == n:
      yield c
   for i in range(1, n+1):
      if i not in c and i not in r[len(c)]:
         yield from row(n, r, c+[i])

def to_latin(n, c = []):
  if len(c) == n:
     yield c
  else:
     for i in row(n, [[]]*n if not c else list(zip(*c))):
        yield from to_latin(n, c+[i])


with open('latin.txt', 'w') as latinfile:
    tablenumber=0
    for i in to_latin(4):    
        for b in i:
            latinfile.write((str(b)))           
            latinfile.write('\n')    
        tablenumber=tablenumber+1
        latinfile.write('TableL=> ')
        latinfile.write(str(tablenumber))
        latinfile.write('\n')
        latinfile.write('.'*12)
        latinfile.write('\n')  
        
    latinfile.close()

print("search in latin.txt")